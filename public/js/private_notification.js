function notifyMe() {
    if (Notification.permission !== "granted")
      Notification.requestPermission();
    else {
        firebase.auth().onAuthStateChanged(function (user) {
            var chatRef = firebase.database().ref('chat_room');
            chatRef.once('value')
            .then(function (snapshot) {
                chatRef.on('child_changed', function (data) {
                    var name;
                    var uid;
                    if(user.uid==data.val().uid1||user.uid==data.val().uid2){
                        if(data.val().name1 == user.name) 
                        {
                            name=data.val().name2;
                            uid=data.val().uid2;
                        }
                        else{ 
                            name = data.val().name1;
                            uid=data.val().uid1;
                        }
                        var notification = new Notification('Private Message', {
                        icon: 'img/private.jpg',
                        body: " From\t" + name  
                        });
                        notification.onclick = function () {
                            window.location.href = "chat.html?" + uid;      
                        };
                    }
                }); 
            })
        })
  
  
    }
}

function show(postsRef){
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Message</h6><div class='media text-muted pt-3'><img src='img/chat.png'  alt='' class='mr-2 rounded' style='height:32px; width:45px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong style='color:black'>";
    var str_after_content = "</p></div></div>\n";

    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    console.log(postsRef);

    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function(childshot){
                first_count += 1
            });
            postsRef.on('child_added', function (data) {
                if (second_count > first_count) {
                    var notification = new Notification('Notification title', {
                      icon: 'img/house.png',
                      body:"Private Message from\t<" + childData.name + ">" ,
                    });
                    notification.onclick = function () {
                      window.location.href = "chat.html";      
                    };
                }
            }); 
        })
    }

notifyMe();