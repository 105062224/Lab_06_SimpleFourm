function init(){
    firebase.auth().onAuthStateChanged(function (user) {
        if(user){
            var user = firebase.auth().currentUser;
            var enter = document.getElementById('enter');
            var cancel = document.getElementById('cancel');
            var name = document.getElementById('name');
            var school = document.getElementById('school');
            var phone = document.getElementById('phone');
            var age = document.getElementById('age');
            var birthday = document.getElementById('birthday');
            var gender = document.getElementById('gender');
            var city = document.getElementById('city');

            firebase.database().ref('/users/' + user.uid).once('value').then(function(snapshot){
                var info_name = snapshot.val().name;
                var info_school = snapshot.val().school;
                var info_phone = snapshot.val().phone;
                var info_age = snapshot.val().age;
                var info_birthday = snapshot.val().birthday;
                var info_gender = snapshot.val().gender;
                var info_city = snapshot.val().city;


                name.setAttribute("value",info_name);
                school.setAttribute("value",info_school);
                phone.setAttribute("value",info_phone);
                age.setAttribute("value",info_age);
                birthday.setAttribute("value",info_birthday);
                gender.setAttribute("value",info_gender);
                city.setAttribute("value",info_city);

            })

            enter.addEventListener('click',function(user){
                var user = firebase.auth().currentUser;

                firebase.database().ref('users/' + user.uid).set(
                {
                    name:name.value,
                    school:school.value,
                    phone:phone.value,
                    age:age.value,
                    birthday: birthday.value,
                    gender : gender.value,
                    city:city.value,
                    uid:user.uid,
                    email:user.email
                });


                window.location.href = 'index.html';
            })

            cancel.addEventListener('click',function(user){
                window.location.href = 'index.html';
            })

         /*   //storage
            var storageRef = firebase.storage().ref();
            var uploadFileInput = document.getElementById("uploadFileInput");
            uploadFileInput.addEventListener("change", function(){
                var file = this.files[0];
                var uploadTask = storageRef.child('images/'+file.name).put(file);
      uploadTask.on('state_changed', function(snapshot){
        // 觀察狀態變化，例如：progress, pause, and resume

        // 取得檔案上傳狀態，並用數字顯示

        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        console.log('Upload is ' + progress + '% done');
        switch (snapshot.state) {
          case firebase.storage.TaskState.PAUSED: // or 'paused'

            console.log('Upload is paused');
            break;
          case firebase.storage.TaskState.RUNNING: // or 'running'

            console.log('Upload is running');
            break;
        }
      }, function(error) {
        // Handle unsuccessful uploads

      }, function() {
        // Handle successful uploads on complete

        // For instance, get the download URL: https://firebasestorage.googleapis.com/...

        user.photoURL = uploadTask.snapshot.downloadURL;
        console.log(uploadTask.snapshot.downloadURL);
      });
        
            },false);

            var img = document.getElementById('img');
            img.setAttribute("src",user.photoURL);
        */





        //////////////////////////////////////////////
        }
    });
}


window.onload = function () {
    console.log('init');
    init();
};