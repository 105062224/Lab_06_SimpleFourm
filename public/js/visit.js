function init(){
    var name = document.getElementById('name');
    var school = document.getElementById('school');
    var phone = document.getElementById('phone');
    var age = document.getElementById('age');
    var birthday = document.getElementById('birthday');
    var gender = document.getElementById('gender');
    var city = document.getElementById('city');
    var back = document.getElementById('back');
    var chat = document.getElementById('chat');

    var uid0 = location.search;
    var uid = uid0.substring(1,uid0.length);
    var road = uid0.substring(uid0.length-1,uid0.length);
    console.log(uid);

    firebase.database().ref('/users/' + uid).once('value').then(function(snapshot){
        var info_name = snapshot.val().name;
            var info_school = snapshot.val().school;
            var info_phone = snapshot.val().phone;
            var info_age = snapshot.val().age;
            var info_birthday = snapshot.val().birthday;
            var info_gender = snapshot.val().gender;
            var info_city = snapshot.val().city;

            if(snapshot.val().name==""||snapshot.val().name==undefined) info_name="保密";
            if(snapshot.val().school==""||snapshot.val().school==undefined) info_school="保密";
            if(snapshot.val().phone==""||snapshot.val().phone==undefined) info_phone="保密";
            if(snapshot.val().age==""||snapshot.val().age==undefined) info_age="保密";
            if(snapshot.val().birthday==""||snapshot.val().birthday==undefined) info_birthday="保密";
            if(snapshot.val().gender==""||snapshot.val().gender==undefined) info_gender="保密";
            if(snapshot.val().city==""||snapshot.val().city==undefined) info_city="保密";
            console.log(info_school);

            name.innerHTML = "Name:\t" + info_name;
            school.innerHTML = "School:\t" + info_school;
            phone.innerHTML = "Phone:\t" + info_phone;
            age.innerHTML = "Age:\t" + info_age;
            birthday.innerHTML = "Birthday:\t" + info_birthday;
            gender.innerHTML = "Gender：\t" + info_gender;
            city.innerHTML = "City:\t" + info_city;
    })

    back.addEventListener('click',function()
    {
       
        window.location.href = "page.html";
      
        window.location.href = "index.html";
    })
    
    chat.addEventListener('click',function()
    {
        window.location.href = "chat.html?" + uid;
    })
}

window.onload = function () {
    init();
};

