function init(){
    var menu = document.getElementById('dynamic-menu');
    var user_email = '';
    var user_name;
    var user_uid;
    //var user_comment;
    var uid0 = location.search;
    var uid = uid0.substring(1,uid0.strlen);
    var name; 
    var ref0 = firebase.database().ref('users/' + uid);
    ref0.once("value")
        .then(function(snapshot) {
            name = snapshot.val().name;
        });
    
    console.log(uid);

    firebase.auth().onAuthStateChanged(function (user) {
        if(user){
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + 
            "</span> <span class='dropdown-item' id='home'>Home</span>" + "<span class='dropdown-item' id='back'>Public Room</span>";
            user_email = user.email;
            user_uid = user.uid;
            var user = firebase.auth().currentUser;
                firebase.database().ref('/users/' + user.uid).once('value').then(function(snapshot) {
                    if(snapshot.val().name == undefined || snapshot.val().name == "") user_name = "Noname";
                    else user_name = snapshot.val().name;
                })

                var chatRef = firebase.database().ref('chat_room');
                var exist = 0;
                var postsRef;
            
                //connect room
                chatRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function(childshot){
                        if((user_uid==childshot.val().uid1 || user_uid==childshot.val().uid2) && (uid==childshot.val().uid1 || uid==childshot.val().uid2)){
                            exist = 1;
                            postsRef = firebase.database().ref('chat_room/' + childshot.key + '/comment');
                            console.log(postsRef);
                            show(postsRef);
                        }
                    })
                    if(exist==0){
                        var data = {
                            uid1:user_uid,
                            uid2:uid,
                            name1:user_name,
                            name2:name
                        }
                        console.log(user_name);
                        chatRef.push(data);
                        init();
                    }
                })
            
                post_btn = document.getElementById('post_btn');
                post_txt = document.getElementById('comment');
            
                post_btn.addEventListener('click', function () {
                    if (post_txt.value != "") {
                        var date = new Date();
                        var post_time =(date.getMonth()+1) + "/" + date.getDate() + " " + date.getHours() + "：" + date.getMinutes();
                        console.log(post_time);
                        //var Ref = firebase.database().ref('com_list');
                        var data = {
                            data: post_txt.value,
                            email: user_email,
                            name: user_name,
                            uid : user_uid,
                            time : post_time
                        };
                        postsRef.push(data);
                        post_txt.value = "";
                    }
                });

            var home = document.getElementById('home');
            home.addEventListener('click',function(){
                window.location.href = "index.html";
            })

            var back = document.getElementById('back');
            back.addEventListener('click',function(){
                console.log('check');
                window.location.href = "page.html";
            })
        }
    })

}

function show(postsRef){
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Message</h6><div class='media text-muted pt-3'><img src='img/chat.png'  alt='' class='mr-2 rounded' style='height:32px; width:45px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong style='color:black'>";
    var str_after_content = "</p></div></div>\n";

    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    console.log(postsRef);

    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function(childshot){
                var data = childshot.val();
                total_post[total_post.length] = str_before_username + data.name + "</strong><span style='padding-left:30px'> (" + data.time + ")<span></br><span  style='font-size:15px'>" + data.data + "</span>" + str_after_content;
                first_count += 1
            });

        
            document.getElementById('post_list').innerHTML = total_post.join('');

            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                //user_comment.push(childData.data);
                    total_post[total_post.length] = str_before_username + childData.name + "</strong><span style='padding-left:30px'> (" + childData.time + ")<span></br><span  style='font-size:15px'>" + childData.data + "</span>" + str_after_content
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
    }

window.onload = function () {
    init();
};