function init() {
    var user_email = '';
    var user_name;
    //var user_comment;
    var user_uid;
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            user_uid = user.uid;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + 
            "</span> <span class='dropdown-item' id='home'>Home</span> <span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var home = document.getElementById('home');
            home.addEventListener('click',function(){
                window.location.href = "index.html";
            })

            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function(e)
                {
                  create_alert("success","sucess");
                }).then(window.location.href = "index.html").catch(function(e)
                {
                  create_alert("error",e.message);
                });
              })

              var user = firebase.auth().currentUser;
              firebase.database().ref('/users/' + user.uid).once('value').then(function(snapshot) {
                if(snapshot.val().name == undefined || snapshot.val().name == "") user_name = "Noname";
                else user_name = snapshot.val().name;
              }).catch(function(){
                alert("Please set the profile first");
                window.location.href = "profile.html";
            })

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
        }
    });


    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var date = new Date();
            var post_time =(date.getMonth()+1) + "/" + date.getDate() + " " + date.getHours() + "：" + date.getMinutes();
            console.log(post_time);
            var Ref = firebase.database().ref('com_list');
            var data = {
                data: post_txt.value,
                email: user_email,
                name: user_name,
                uid : user_uid,
                time : post_time
            };
            Ref.push(data);
            post_txt.value = "";
        }
    });

    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Message</h6><div class='media text-muted pt-3'><img src='img/chat.png'  alt='' class='mr-2 rounded' style='height:32px; width:45px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong style='color:black'>";
    var str_after_content = "</p></div></div>\n";


    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function(childshot){
                var data = childshot.val();
                if(user_uid == data.uid)
                    total_post[total_post.length] = str_before_username + data.name + "</strong><span style='padding-left:30px'> (" + data.time + ")<span></br><span  style='font-size:15px'>" + data.data + "</span>" + str_after_content;
                else
                total_post[total_post.length] = str_before_username + "<a href='visit.html?" + data.uid + " '> " + data.name + "</a></strong><span style='padding-left:30px'> (" + data.time + ")<span></br><span  style='font-size:15px'>" + data.data + "</span>" + str_after_content;
                first_count += 1
            });

            
            document.getElementById('post_list').innerHTML = total_post.join('');

            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    //user_comment.push(childData.data);
                    if(user_uid == childData.uid)
                        total_post[total_post.length] = str_before_username + childData.name + "</strong><span style='padding-left:30px'> (" + childData.time + ")<span></br><span  style='font-size:15px'>" + childData.data + "</span>" + str_after_content;
                    else
                        total_post[total_post.length] = str_before_username + "<a href='visit.html?" + childData.uid +"'>" + childData.name + "</a></strong><span style='padding-left:30px'> (" + childData.time + ")<span></br><span  style='font-size:15px'>" + childData.data + "</span>" + str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
}

window.onload = function () {
    init();
};